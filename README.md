# pumpkin-supmcu-i2cdriver Package

The `pumpkin-supmcu-i2cdriver` package has the following functionality:

* Leverages business logic found in [pumpkin-supmcu](https://pumpkin-supmcu.readthedocs.io/en/latest/index.html).
    + Please read `pumpkin-supmcu` documentation for usage, this is the implementation package for [I2C Driver](https://i2cdriver.com/)
* Interfaces `pumpkin-supmcu` package with the [I2C Driver](https://i2cdriver.com/)

The documentation for the `pumpkin-supmcu-i2cdriver` package can be [found here](https://pumpkin-supmcu-i2cdriver.readthedocs.io/en/latest/).
