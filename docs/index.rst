.. Pumpkin SupMCU Interface documentation master file, created by
   sphinx-quickstart on Tue Nov  5 17:47:45 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

`pumpkin-supmcu` documentation
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   api/i2c

The ``pumpkin-supmcu-i2cdriver`` package has the following functionality:

* Leverages business logic found in `pumpkin-supmcu <https://pumpkin-supmcu.readthedocs.io/en/latest/index.html>`_.
    + Please read ``pumpkin-supmcu`` documentation for usage, this is the implementation package for `I2C Driver <https://i2cdriver.com/>`_
* Interfaces ``pumpkin-supmcu`` package with the `I2C Driver <https://i2cdriver.com/>`_

The documentation for the ``pumpkin_supmcu`` package can be `found here <https://pumpkin-supmcu.readthedocs.io/en/latest/index.html>`_.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
